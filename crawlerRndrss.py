#!/usr/bin/env python
# coding: utf-8

#-*- coding:utf-8 -*-
from bs4 import BeautifulSoup
import time
import pandas as pd
import datetime
import re
from bs4 import BeautifulSoup as bs
import requests
import csv
import numpy as np
import mysql.connector
import pymysql
from sqlalchemy import create_engine
import threading


now = time.localtime()
ntime= "%04d%02d%02d_%02d:%02d" % (now.tm_year, now.tm_mon, now.tm_mday, now.tm_hour, now.tm_min)


page = requests.get('https://rch.hufs.ac.kr/jsp/rchnew/ntis/unRndRssList.jsp') 
time.sleep(3)
bsobj = BeautifulSoup(page.text,'html.parser')
table = bsobj.find("div",{"class":"table list margin_top20"})
data=[]

for a in table.find_all('tr'):
    infolist=[]
    #info = tr.get_text()
    #print('info-> ',info)
    for b in a.find_all('td'):
        info = b.get_text()
        infolist.append(info)
        links = a.find_all("a")
        ahref = []
        for a in links:
            href = a.attrs['href']
            infolist.append(href[19:-2])
            #print(href)
    data.append(infolist)

del data[0]
data = pd.DataFrame(data,columns=['company','link','title','date'])
print(data)
print("pd ok")



words = '예측|모델|데이터|빅데이터|인공지능|모델링|AI|기계학습|딥페이크|딥러닝|임베딩'
resultlist=[]
new_list=[]

new_list = data[data['title'].str.contains(words)].head()
resultlist = new_list.sort_values(by=['date']).reset_index(drop = True)

          
print(resultlist)
# print(len(resultlist))
pymysql.install_as_MySQLdb()
engine = create_engine('mysql://root:'+'djslzja'+'@192.168.1.125/crawler_test',encoding='utf-8')
resultlist.to_sql('rndrss1',engine,if_exists='append',index=False, index_label=None, chunksize=500)
print("ok")
